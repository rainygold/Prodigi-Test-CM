import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class ImageCreator {// separated from findColour for testing purposes

    static BufferedImage generateImageFromURL(String imageURL) {

        // remove form post info
        String newURL = imageURL.replace("imgurl=", "");

        // create a new image from the clean url
        try {
            URL url = new URL(newURL);
            return ImageIO.read(url);
        } catch (IOException e) {
            System.out.println(e);
        }

        return null;
    }
}