import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class JsonObjectCreator {
    static JsonObject findColourOfImage(String imageURL) {


        // Image generation
        if (imageURL.contains(".png") || (imageURL.contains(".jpg") || (imageURL.contains(".PNG") || (imageURL.contains(".JPG"))))) {

            // Image generation
            BufferedImage imgToBeUsed = ImageCreator.generateImageFromURL(imageURL);

            // Colour check
            assert imgToBeUsed != null;
            JsonObject JsonToBeReturned = createJsonObject(imgToBeUsed);
            if (JsonToBeReturned != null) return JsonToBeReturned;

        } else {
            JsonObject BadInput = new JsonObject();
            BadInput.addProperty("Rule:", "Submit a proper image url.");
            return BadInput;

        }

        return null;
    }

    private static JsonObject createJsonObject(BufferedImage imgToBeUsed) {

        // Store RGB values of certain colours
        // teal map
        HashMap<String, Object> tealRGBValues = new HashMap<>();
        tealRGBValues.put("key", "teal");
        tealRGBValues.put("red", 0);
        tealRGBValues.put("green", 98);
        tealRGBValues.put("blue", 110);

        // navy map
        HashMap<String, Object> navyRGBVAlues = new HashMap<>();
        navyRGBVAlues.put("key", "navy");
        navyRGBVAlues.put("red", 0);
        navyRGBVAlues.put("green", 0);
        navyRGBVAlues.put("blue", 80);

        // grey map
        HashMap<String, Object> greyRGBValues = new HashMap<>();
        greyRGBValues.put("key", "grey");
        greyRGBValues.put("red", 56);
        greyRGBValues.put("green", 70);
        greyRGBValues.put("blue", 87);

        // black map
        HashMap<String, Object> blackRGBValues = new HashMap<>();
        blackRGBValues.put("key", "black");
        blackRGBValues.put("red", 36);
        blackRGBValues.put("green", 36);
        blackRGBValues.put("blue", 36);

        // none match map
        HashMap<String, Object> noMatchValues = new HashMap<>();
        noMatchValues.put("key", "no colour was matched");

        // colour counters && gson
        int tealCounter = 0;
        int navyCounter = 0;
        int greyCounter = 0;
        int blackCounter = 0;

        // match booleans
        boolean tealRed;
        boolean tealGreen;
        boolean tealBlue;

        boolean navyRed;
        boolean navyGreen;
        boolean navyBlue;

        boolean greyRed;
        boolean greyGreen;
        boolean greyBlue;

        boolean blackRed;
        boolean blackGreen;
        boolean blackBlue;

        // no exact match counters
        HashMap<String, Integer> difference = new HashMap<>();
        final int maxDifference = 10;
        int closestDifference = 10;
        String nameOfKey = "";

        // containers for rgb values of each pixel
        int red;
        int green;
        int blue;
        Color color; // allows access to image's RGB values
        Gson gson = new Gson(); // google library to convert HashMap into JSON

        for (int i = 0; i < imgToBeUsed.getWidth(); i++) {

            for (int j = 0; j < imgToBeUsed.getHeight(); j++) {

                color = new Color(imgToBeUsed.getRGB(i, j));
                red = color.getRed();
                green = color.getGreen();
                blue = color.getBlue();

                // clear hashmap of values for easier comparison
                difference.clear();

                // reset bools
                tealRed = false;
                tealGreen = false;
                tealBlue = false;

                navyRed = false;
                navyGreen = false;
                navyBlue = false;

                greyRed = false;
                greyGreen = false;
                greyBlue = false;

                blackRed = false;
                blackGreen = false;
                blackBlue = false;


                // increment respective counter to find most dominant colour
                if (red == (int) tealRGBValues.get("red") && (green == (int) tealRGBValues.get("green") && (blue == (int) tealRGBValues.get("blue")))) {
                    tealCounter++;
                } else if (red == (int) navyRGBVAlues.get("red") && (green == (int) navyRGBVAlues.get("green") && (blue == (int) navyRGBVAlues.get("blue")))) {
                    navyCounter++;
                } else if (red == (int) greyRGBValues.get("red") && (green == (int) greyRGBValues.get("green") && (blue == (int) greyRGBValues.get("blue")))) {
                    greyCounter++;
                } else if (red == (int) blackRGBValues.get("red") && (green == (int) blackRGBValues.get("green") && (blue == (int) blackRGBValues.get("blue")))) {
                    blackCounter++;
                } else {

                    // no exact matches, therefore find the closest reference match (black, navy, teal, grey)
                    // difference of 10 is the limit (is teal still teal if it is 15 points different  in blue?)
                    if (red - (int) tealRGBValues.get("red") < maxDifference && red - (int) tealRGBValues.get("red") > 0) {
                        difference.put("teal's red", red - (int) tealRGBValues.get("red"));
                    } else if (red - (int) navyRGBVAlues.get("red") < maxDifference) {
                        difference.put("navy's red", red - (int) navyRGBVAlues.get("red"));
                    } else if (red - (int) greyRGBValues.get("red") < maxDifference) {
                        difference.put("grey's red", red - (int) greyRGBValues.get("red"));
                    } else if (red - (int) blackRGBValues.get("red") < maxDifference) {
                        difference.put("black's red", red - (int) blackRGBValues.get("red"));
                    }
                    // iterate through values and find the closest pair
                    for (Map.Entry<String, Integer> entry : difference.entrySet()) {
                        String key = entry.getKey();
                        int value = entry.getValue();

                        if (value < closestDifference) {
                            closestDifference = value;
                            nameOfKey = key;
                        }
                    }

                    // reset for next colour
                    closestDifference = 10;

                    if (nameOfKey.contains("teal's red")) {
                        tealRed = true;
                    } else if (nameOfKey.contains("navy's red")) {
                        navyRed = true;
                    } else if (nameOfKey.contains("black's red")) {
                        blackRed = true;
                    } else if (nameOfKey.contains("grey's red")) {
                        greyRed = true;
                    }


                    if (green - (int) tealRGBValues.get("green") < maxDifference) {
                        difference.put("teal's green", green - (int) tealRGBValues.get("green"));
                    } else if (green - (int) navyRGBVAlues.get("green") < maxDifference) {
                        difference.put("navy's green", green - (int) navyRGBVAlues.get("green"));
                    } else if (green - (int) greyRGBValues.get("green") < maxDifference) {
                        difference.put("grey's green", green - (int) greyRGBValues.get("green"));
                    } else if (green - (int) blackRGBValues.get("green") < maxDifference) {
                        difference.put("black's green", green - (int) blackRGBValues.get("green"));
                    }

                    // iterate through values and find the closest pair
                    for (Map.Entry<String, Integer> entry : difference.entrySet()) {
                        String key = entry.getKey();
                        int value = entry.getValue();

                        if (value < closestDifference) {
                            closestDifference = value;
                            nameOfKey = key;
                        }
                    }

                    // reset for next colour
                    closestDifference = 10;

                    if (nameOfKey.contains("teal's green")) {
                        tealGreen = true;
                    } else if (nameOfKey.contains("navy's green")) {
                        navyGreen = true;
                    } else if (nameOfKey.contains("black's green")) {
                        blackGreen = true;
                    } else if (nameOfKey.contains("grey's green")) {
                        greyGreen = true;
                    }


                    if (blue - (int) tealRGBValues.get("blue") < maxDifference) {
                        difference.put("teal's blue", blue - (int) tealRGBValues.get("blue"));
                    } else if (blue - (int) navyRGBVAlues.get("blue") < maxDifference) {
                        difference.put("navy's blue", blue - (int) navyRGBVAlues.get("blue"));
                    } else if (blue - (int) greyRGBValues.get("blue") < maxDifference) {
                        difference.put("grey's blue", blue - (int) greyRGBValues.get("blue"));
                    } else if (blue - (int) blackRGBValues.get("blue") < maxDifference) {
                        difference.put("black's blue", blue - (int) blackRGBValues.get("blue"));
                    }

                    // iterate through values and find the closest pair
                    for (Map.Entry<String, Integer> entry : difference.entrySet()) {
                        String key = entry.getKey();
                        int value = entry.getValue();

                        if (value < closestDifference) {
                            closestDifference = value;
                            nameOfKey = key;
                        }
                    }

                    // reset for next colour
                    closestDifference = 10;

                    if (nameOfKey.contains("teal's blue")) {
                        tealBlue = true;
                    } else if (nameOfKey.contains("navy's blue")) {
                        navyBlue = true;
                    } else if (nameOfKey.contains("black's blue")) {
                        blackBlue = true;
                    } else if (nameOfKey.contains("grey's blue")) {
                        greyBlue = true;
                    }

                    if (tealRed && tealGreen && tealBlue) {
                        tealCounter++;
                    } else if (navyRed && navyGreen && navyBlue) {
                        navyCounter++;
                    } else if (greyRed && greyGreen && greyBlue) {
                        greyCounter++;
                    } else if (blackRed && blackGreen && blackBlue) {
                        blackCounter++;
                    }
                }
            }

            // gson allows easy conversion
            if (tealCounter > blackCounter && tealCounter > greyCounter && tealCounter > navyCounter) {
                return gson.toJsonTree(tealRGBValues).getAsJsonObject();
            } else if (navyCounter > tealCounter && navyCounter > blackCounter && navyCounter > greyCounter) {
                return gson.toJsonTree(navyRGBVAlues).getAsJsonObject();
            } else if (greyCounter > tealCounter && greyCounter > navyCounter && greyCounter > blackCounter) {
                return gson.toJsonTree(greyRGBValues).getAsJsonObject();
            } else if (blackCounter > tealCounter && blackCounter > navyCounter && blackCounter > greyCounter) {
                return gson.toJsonTree(blackRGBValues).getAsJsonObject();
            }
        }

        return gson.toJsonTree(noMatchValues).getAsJsonObject();
    }
}