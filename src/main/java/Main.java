/*
Charlie Murphy - rainygold
Updated 26/04/2018
 */

import spark.Request;

import java.net.URLDecoder;

import static spark.Spark.get;
import static spark.Spark.port;

public class Main {

    public static void main(String[] args) {

        port(getHerokuAssignedPort()); // Heroku assigns a random port for each deploy
        get("/", (req, res) -> renderBoxPage(req)); // ensure the form is correctly rendered
        get("/image", (req, res) -> {
            String result = URLDecoder.decode(req.queryString(), "UTF-8"); // prevent null error
            return JsonObjectCreator.findColourOfImage(result);
        }); // takes form submission
    }

    // Places the input form on a blank page
    private static String renderBoxPage(Request input) {
        return " Please input the image URL: \n" +
                "<form action=\"/image\" method=\"/post\" >\n" +
                "  Image URL: <input type=\"text\" name=\"imgurl\"><br>\n" +
                "  <input type=\"submit\" value=\"Submit\" required>\n" +
                "</form> ";
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

}