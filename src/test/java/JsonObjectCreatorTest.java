import com.google.gson.JsonObject;

import static org.junit.jupiter.api.Assertions.assertTrue;

class JsonObjectCreatorTest {

    // Simple test case to demonstrate usage of JUnit
    @org.junit.jupiter.api.Test
    void findColourOfImage() {

        // Tests invalid case
        JsonObject InvalidJsonToTest = JsonObjectCreator.findColourOfImage("test-pm");
        assert InvalidJsonToTest != null;
        System.out.println(InvalidJsonToTest.get("Rule:"));
        assertTrue(InvalidJsonToTest.get("Rule:").getAsString().equalsIgnoreCase("Submit a proper image url."));

        // Test valid case
        JsonObject ValidJsonToTest = JsonObjectCreator.findColourOfImage("https://pwintyimages.blob.core.windows.net/samples/stars/sample-teal.png");
        assert ValidJsonToTest != null;
        System.out.println(ValidJsonToTest.get("key"));
        assertTrue(ValidJsonToTest.get("key").getAsString().equalsIgnoreCase("teal"));
    }
}